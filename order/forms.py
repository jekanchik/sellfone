# -*-coding:utf8-*-

from django import forms
from django.db.models import Q, Min


from .models import Order, OrderType


class MyModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj


class ChoseOrderTypeForm(forms.ModelForm):
    order_type = MyModelChoiceField(widget=forms.RadioSelect, queryset=OrderType.objects.annotate(min_price=Min('childrens__additional_price')), empty_label=None)

    def __init__(self, *args, **kwargs):
        super(ChoseOrderTypeForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Order
        fields = ('order_type',)


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ('order_type', 'product', 'user', 'sessionId', 'unique_order_id')

        labels = {
            'first_name': u'Ваше имя',
            'last_name': u'Ваша фамилия',
            'phone': u'Номер телефона',
            'email': u'Электронный адрес',
            'city': u'Город',
            'shipping_type': u'Способ доставки',
            'shipping_department': u'Отделение',
            'payment_type': u'Способ оплаты'
        }

        widgets ={
            'first_name': forms.TextInput(attrs={'placeholder':u'Введите имя', 'class':'phone-cheract addtnl-ch'}),
            'last_name': forms.TextInput(attrs={'placeholder':u'Введите фамилию', 'class':'phone-cheract addtnl-ch'}),
            'phone': forms.TextInput(attrs={'placeholder':u'+38 012 345 67 89', 'class':'phone-cheract addtnl-ch'}),
            'email': forms.TextInput(attrs={'placeholder': u'sell@fone.com', 'class': 'phone-cheract addtnl-ch'}),
            'city': forms.Select(attrs={'class': u'phone-cheract'}),
            'shipping_type': forms.Select(attrs={'class': u'bold phone-cheract'}),
            'shipping_department': forms.Select(attrs={'class': u'addtnl-ch phone-cheract'}),
            'payment_type': forms.Select(attrs={'class': u'addtnl-ch phone-cheract'})
        }