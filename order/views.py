# -*-coding:utf8-*-
from __future__ import unicode_literals

import json
import requests

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.db.models import Q
from django.views.generic import TemplateView

from products.models import Product

from .forms import ChoseOrderTypeForm, OrderForm
from .models import OrderType, Order


# Create your views here.


def ChoseOrderType(request, product_pk):
    product = get_object_or_404(Product, pk=product_pk)
    ctx = {'product': product}
    if request.POST or request.is_ajax():
        form = ChoseOrderTypeForm(request.POST)
        if form.is_valid():
            order_type = form.cleaned_data['order_type']
            return redirect('order:order_create', product_pk=product.pk, order_type_pk=order_type.pk)

        ctx.update({'form': form})

    else:
        ctx.update({'form': ChoseOrderTypeForm()})
    return render(request, 'order_type.html', ctx)


def OrderCreate(request, product_pk, order_type_pk):
    product = get_object_or_404(Product, pk=product_pk)
    order_type = get_object_or_404(OrderType, Q(pk=order_type_pk), Q(structure=1) | Q(structure=3))
    total_price = order_type.additional_price + product.price if order_type.additional_price else product.price
    ctx = {'product': product, 'order_type': order_type, 'total_price': total_price}
    if request.POST:
        form = OrderForm(request.POST)
        if form.is_valid():
            order = form.save(commit=False)
            uapay = UaPay(total_price)
            order.product = product
            order.order_type = order_type
            order.sessionId = uapay.get_sessionId()
            order.save()
            return redirect(uapay.get_paymentPageUrl(order.pk))
        else:
            ctx.update({'form': form})
    else:
        ctx.update({'form': OrderForm()})

    return render(request, 'order_form.html', ctx)


class UaPay(object):
    clientId = '1'

    def __init__(self, total_price):
        self.total_price = int(total_price*100)

    def get_sessionId(self):
        params = {
            'params':
                {'clientId': self.clientId},
        }
        response = requests.post('http://api.demo.uapay.ua/api/sessions/create', json=params)
        if response.status_code == 200 and response.json().get('status'):
            self.sessionId = response.json().get('data')['id']
            return self.sessionId

    def get_paymentPageUrl(self, order_id):
        params = {
                "params": {
                    "sessionId": self.sessionId,
                },
                "data": {
                    "type": "PAY",
                    "externalId": str(order_id),
                    "description": "Test payment",
                    "amount": self.total_price,
                    "userRedirectUrl": str(reverse_lazy('order:done_pay')), #TODO ADD domen name
                    "callbackUrl": "http://api.exapmle.com/callback", #TODO add callback url and view
                    "extraInfo": "{}",
                    "currency": 980
                }
            }
        response = requests.post('http://api.demo.uapay.ua/api/orders/create', json=params)
        if response.status_code == 200 and response.json().get('status'):
                paymentPageUrl = response.json().get('data')['paymentPageUrl']
                unique_order_id = response.json().get('data')['id']
                Order.objects.filter(id=order_id).update(unique_order_id=unique_order_id)
                return paymentPageUrl
        return reverse_lazy('order:error_pay')


class ErrorPay(TemplateView):
    template_name = 'error_pay.html'


class DonePay(TemplateView):
    template_name = 'done_pay.html'
