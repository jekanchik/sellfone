# -*-coding:utf8-*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError

from users.models import User
from products.models import Product

# Create your models here.


class OrderType(models.Model):

    class SECURITY_LEVEL:
        BASIC = 'normal'
        HIGH = 'good'
        MAX = 'perfect'

        CHOICES = (
            (BASIC, _(u'базовый')),
            (HIGH, _(u'высокий')),
            (MAX, _(u'максимальный'))
        )

    STRUCTURE_CHOICES = (
        (1, _(u'Одиночный тип')),
        (2, _(u'Родительский тип')),
        (3, _(u'Дочерний тип'))
    )

    title = models.CharField(_(u'Название'), max_length=300)
    security_level = models.CharField(_(u'Уровень безопасности'),max_length=20, choices=SECURITY_LEVEL.CHOICES, blank=True, null=True)
    description = models.TextField(_(u'Описание'), blank=True, null=True)

    structure = models.IntegerField(choices=STRUCTURE_CHOICES)

    parent = models.ForeignKey(
        'self',
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='childrens'
    )

    additional_price = models.IntegerField(_(u'Наценка'), blank=True, null=True)

    class Meta:
        verbose_name = _(u'Тип заказа')
        verbose_name_plural = _(u'Типы заказа')

    @property
    def is_standalone(self):
        return 1 == self.structure

    @property
    def is_parent(self):
        return 2 == self.structure

    @property
    def is_child(self):
        return 3 == self.structure

    def clean(self):
        if not self.is_child and self.parent:
            raise ValidationError(_(u'Родительский или одиночный тип не может иметь родителя'))

        if self.is_parent and self.additional_price:
            raise ValidationError(_(u'Родительский тип не может иметь наценку'))


    def __unicode__(self):
        if not self.is_child:
            return self.title
        else:
            return '%s > %s'%(self.parent.title, self.title)


class ShippingCity(models.Model):
    name = models.CharField(_(u'Название'), max_length=100)

    def __unicode__(self):
        return self.name

class ShippingType(models.Model):
    name = models.CharField(_(u'Название'), max_length=100)

    def __unicode__(self):
        return self.name


class ShippingDepartment(models.Model):
    name = models.CharField(_(u'Название'), max_length=100)
    shipping_posht = models.ForeignKey(ShippingType, verbose_name=_(u'Доставка'), related_name='posht_departments')
    shipping_city = models.ForeignKey(ShippingCity, verbose_name=_(u'Город'), related_name='city_departments')

    def __unicode__(self):
        return self.name


class Order(models.Model):

    PAYMENT_TYPE_CHOICES = (
        (1, u'Оплата картой'),
    )

    when_created = models.DateTimeField(_('Дата создания'), auto_now_add=True)

    order_type = models.ForeignKey(OrderType, verbose_name=_(u'Тип сделки'))

    product = models.ForeignKey(Product, verbose_name=_(u'Товар'))

    user = models.ForeignKey(
        User,
        verbose_name=_(u'Пользователь'),
        blank=True,
        null=True,
        related_name='orders'
    )

    first_name = models.CharField(_(u'Имя'), max_length=30)
    last_name = models.CharField(_(u'Фамилия'), max_length=30)
    phone = models.CharField(_('Номер телефона'), max_length=16,
                             validators=[RegexValidator(r'^\+\d{9,15}$')])

    email = models.EmailField(_(u'Электронный адрес'))
    city = models.ForeignKey(
        ShippingCity,
        verbose_name= _(u'Город')
    )

    shipping_type = models.ForeignKey(ShippingType, verbose_name=_(u'Способ доставки'))
    shipping_department = models.ForeignKey(ShippingDepartment, verbose_name=_(u'Отделение'))

    payment_type = models.IntegerField(verbose_name=_(u'Тип оплаты'), choices=PAYMENT_TYPE_CHOICES)

    sessionId = models.CharField(
        verbose_name='UAPAY-sessionId',
        max_length=250,
        blank=True,
        null=True
    )
    unique_order_id= models.CharField(
        verbose_name='UAPAY-unique-Order-Id',
        max_length=250,
        blank=True,
        null=True
    )

    def clean(self):
        if hasattr(self, 'order_type') and self.order_type.is_parent:
            raise ValidationError(_(u'Родительский тип не может относиться к заказу, выбирите один из его дочерних типов'))


    @property
    def total_price(self):
        if self.order_type.additional_price:
            return self.product.price + self.order_type.additional_price
        else:
            return self.product.price

