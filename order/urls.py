
from django.conf.urls import url, include
from order import views
# from .views import AddProductView


urlpatterns = [
    url(
        r'^create_order/(?P<product_pk>\d+)/$',
        views.ChoseOrderType,
        name='chose_order_type'
    ),
    url(
        r'^create_order/(?P<product_pk>\d+)/(?P<order_type_pk>\d+)/$',
        views.OrderCreate,
        name='order_create'
    ),
    url(r'^error_pay/$', views.ErrorPay.as_view(), name='error_pay'),
    url(r'^done_pay/$', views.DonePay.as_view(), name='done_pay'),
]
