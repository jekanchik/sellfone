from django.contrib import admin

from .models import *
# Register your models here.


admin.site.register(OrderType)
admin.site.register(ShippingCity)
admin.site.register(ShippingType)
admin.site.register(ShippingDepartment)
admin.site.register(Order)