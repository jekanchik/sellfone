# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseForbidden
from django.views.generic import TemplateView, UpdateView

from users.forms import UserUpdateForm
from users.models import User


class Cabinet(TemplateView):
    template_name = 'users/cabinet.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        return super(Cabinet, self).dispatch(request, *args, **kwargs)


class ProfileUpdateView(UpdateView):
    template_name = 'users/profile_update.html'
    model = User
    # fields = ['username']
    form_class = UserUpdateForm

    def get_object(self, queryset=None):
        return self.request.user

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        return super(ProfileUpdateView, self).dispatch(request, *args, **kwargs)


class ProfileRatingView(TemplateView):
    template_name = 'users/profile_rating.html'


class ProfilePurchasesView(TemplateView):
    template_name = 'users/profile_purchases.html'


class ProfileSalesView(TemplateView):
    template_name = 'users/profile_sales.html'


class ProfileSettingsView(TemplateView):
    template_name = 'users/profile_settings.html'
