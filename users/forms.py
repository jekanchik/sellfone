# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.forms import Select, TextInput

from users.models import User


class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('email', 'phone', 'first_name', 'last_name', 'username', 'place', 'photo', 'shipping_address')
        widgets = {
            'place': Select(attrs={'class': 'phone-cheract bold'}),
            'first_name': TextInput(attrs={'class': 'phone-cheract bold'}),
            'last_name': TextInput(attrs={'class': 'phone-cheract bold'}),
            'email': TextInput(attrs={'class': 'phone-cheract bold'}),
            'phone': TextInput(attrs={'class': 'phone-cheract bold'}),
        }