# -*- coding: utf-8 -*-

from allauth.account.forms import LoginForm, SignupForm


def registration_login(request):
    """
    Добавляет allauth LoginForm, SignupForm

    """
    return {
        'signup_form': SignupForm(),
        'login_form': LoginForm(),
    }
