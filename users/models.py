# coding=utf-8
# -*- coding; utf-8 -*-
from __future__ import unicode_literals

import os
import urllib

from allauth.account.signals import user_signed_up
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.files import File
from django.core.validators import RegexValidator
from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext as _

from world.models import Territory


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, phone, password, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, phone=phone, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, phone=None, password=None,
                    **extra_fields):
        for field in ('is_staff', 'is_superuser'):
            extra_fields.setdefault(field, False)
        return self._create_user(email, phone, password, **extra_fields)

    def create_superuser(self, email, phone, password, **extra_fields):
        for field in ('is_staff', 'is_superuser'):
            if extra_fields.setdefault(field, True) is not True:
                raise ValueError('Superuser must have %s=True' % field)
        return self._create_user(email, phone, password, **extra_fields)


class User(AbstractUser):
    objects = UserManager()

    username_validator = UnicodeUsernameValidator()

    first_name = models.CharField(_('first name'), max_length=30)
    username = models.CharField(
        _('username'),
        max_length=150,
        help_text=_(
            '150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator], blank=True, null=True
    )
    email = models.EmailField(_('email address'), unique=True)
    phone = models.CharField(_('Номер телефона'), max_length=16,
                             validators=[RegexValidator(r'^\+\d{9,15}$')])
    place = models.ForeignKey(Territory, verbose_name=_('Населённый пункт'),
                              related_name='users', blank=True, null=True)
    photo = models.ImageField(_('Фото'), upload_to='users',
                              blank=True, null=True)
    shipping_address = models.CharField(_('Адрес доставки'), max_length=255,
                                        blank=True, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone', 'first_name']


class Message(models.Model):
    sender = models.ForeignKey(User, verbose_name=_('Отправитель'),
                               related_name='outbox')
    receiver = models.ForeignKey(User, verbose_name=_('Получатель'),
                                 related_name='inbox')
    message = models.TextField(_('Teкст сообщения'))
    created_at = models.DateTimeField(_('Дата отправки'), auto_now_add=True)

    class Meta:
        verbose_name = _('Сообщение')
        verbose_name_plural = _('Сообщения')

    def __unicode__(self):
        return ' -> '.join(map(str, (self.sender, self.receiver)))


@receiver(user_signed_up)
def registered(request, user, sociallogin=None, **kwargs):
    if sociallogin:
        data = sociallogin.account.extra_data
        provider = sociallogin.account.provider

        def load_user_photo(url):
            result = urllib.urlretrieve(url)
            file_name = '{}/{}_{}'.format(provider,data['id'] , os.path.basename(url))
            if provider == 'facebook':
                file_name += '.jpg'
            user.photo.save(file_name, File(open(result[0])))

        if provider == 'twitter':
            url = data['profile_image_url'].replace('normal', '400x400')
            load_user_photo(url)

        if provider == 'facebook':
            user.first_name = data['first_name']
            user.last_name = data['last_name']
            # user.username = 'facebook' + str(data['id'])
            url = "https://graph.facebook.com/{}/picture?type=large".format(data['id'])
            load_user_photo(url)

        if provider == 'google':
            url = data['picture']
            load_user_photo(url)

        user.save()
