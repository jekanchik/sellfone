from django import template

register = template.Library()

@register.simple_tag(takes_context=True)
def user_progress(context, user, float=False):
    progres = 0
    if user.first_name:
        progres += 10
    if user.last_name:
        progres += 10
    if user.photo:
        progres += 10
    if user.place:
        progres += 10
    if user.email:
        progres += 10
    if user.shipping_address:
        progres += 10
    if user.phone:
        progres += 10
    if user.socialaccount_set.all():
        progres += 10
    if float:
        progres /= 100.0
    return progres