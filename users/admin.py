from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.utils.html import format_html
from django.utils.translation import ugettext as _

from users.models import Message, User
from world.admin import CountryFilter


class UserChangeForm(auth_admin.UserChangeForm):

    class Meta(auth_admin.UserChangeForm.Meta):
        model = User


class UserCreationForm(auth_admin.UserCreationForm):

    class Meta(auth_admin.UserCreationForm.Meta):
        model = User
        fields = ('first_name', 'phone', 'email')


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'username',
                                         'phone', 'photo')}),
        (_('Places'), {'fields': ('place', 'shipping_address')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions'),
                            'classes': ('collapse',)}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'phone', 'first_name',
                       'password1', 'password2'),
        }),
    )
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ('username', 'phone', 'mailto', 'first_name', 'last_name',
                    'place', 'is_staff')
    list_filter = (('place', CountryFilter), 'is_staff', 'is_superuser',
                   'is_active', 'groups')
    search_fields = ('username', 'first_name', 'last_name', 'email', 'phone')
    ordering = ('username',)
    filter_horizontal = ('groups', 'user_permissions',)

    def mailto(self, obj):
        return format_html('<a href="mailto:{email}">{email}</a>',
                           email=obj.email)
    mailto.short_description = 'Email'


class MessageAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'sender', 'receiver')
    date_hierarchy = 'created_at'
admin.site.register(Message, MessageAdmin)
