from django.conf.urls import url, include
from users import views


urlpatterns = [
    url(
        r'^$',
        views.Cabinet.as_view(),
        name='cabinet'
    ),
    url(
        r'^update/$',
        views.ProfileUpdateView.as_view(),
        name='update'
    ),
    url(
        r'^rating/$',
        views.ProfileRatingView.as_view(),
        name='rating'
    ),
    url(
        r'^purchases/$',
        views.ProfilePurchasesView.as_view(),
        name='purchases'
    ),
    url(
        r'^sales/$',
        views.ProfileSalesView.as_view(),
        name='sales'
    ),
    url(
        r'^settings/$',
        views.ProfileSettingsView.as_view(),
        name='settings'
    ),
]