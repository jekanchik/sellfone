# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _

# Create your models here.
from devices.models import VersionModel


class TablePrice(models.Model):
    device = models.ForeignKey(
        VersionModel,
        verbose_name=_('Вариант модели'),
        related_name='tab_price'
    )
    when_created = models.DateTimeField(
        verbose_name=_('Дата создания'),
        auto_now_add=True
    )
    price = models.DecimalField(
        _('Цена'),
        decimal_places=2,
        max_digits=9
    )

    class Meta:
        verbose_name = _('Таблица цен')
        verbose_name_plural = _('Таблицы цен')

    def __unicode__(self):
        return '%s' % self.device
