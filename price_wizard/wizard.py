# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class WizardPrice(object):
    recommended_price = 0.1

    def __init__(self, product=None, device=None, device_proxy=None):
        self.product = product
        self.device = device
        self.device_proxy = device_proxy
        if self.product:
            self._get_recommended_price()
        elif self.device:
            self._get_recomended_price_anonimus()
        else:
            raise Exception("Must present Device or Product")

    def _get_recomended_price_anonimus(self):
        price = self.device.tab_price.all().latest('when_created').price
        self.recommended_price = int(round(float(price) * self._get_coef_how_works(self.device_proxy) * self._get_coef_damage(
                self.device_proxy.damage), -1))

    def _get_recommended_price(self):
        try:
            price = self.product.device.version_model.first().tab_price.all().latest('when_created').price
            self.recommended_price = int(round(float(price) * self._get_coef_how_works(self.product) * self._get_coef_damage(
                self.product.damage), -1))
        except:
            pass

    def _get_coef_how_works(self, product):
        if product.is_fully_exploitable:
            return 0.85
        elif product.broken_screen:
            return 0.35
        else:
            return 0.55

    def _get_coef_damage(self, data):
        damage = {
            0: 0.9,
            1: 0.8,
            2: 0.7,
            3: 0.5,
            4: 0.3
        }
        return damage.get(data)
