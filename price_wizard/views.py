# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from dal import autocomplete
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.views.generic import UpdateView, TemplateView

from devices.models import Device, Color, MemSize, Brand, DeviceType
from price_wizard.forms import ExpressCostForm
from price_wizard.wizard import WizardPrice

from products.models import Product


class DeviceProxy(object):
    is_fully_exploitable = None
    broken_screen = None
    damage = None


class ExpressCostView(TemplateView):
    form_class = ExpressCostForm
    template_name = 'products/express_cost_estimation.html'

    def get(self, request, *args, **kwargs):
        if self.request.GET.get('email'):
            self.template_name = 'products/include/express_cost_include.html'
        return super(ExpressCostView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ExpressCostView, self).get_context_data(**kwargs)
        context['form'] = self.form_class(self.request.GET)
        context['type'] = get_object_or_404(DeviceType, id=self.request.GET.get('type'))
        context['brand'] = get_object_or_404(Brand, id=self.request.GET.get('brand'))
        context['device'] = get_object_or_404(Device, id=self.request.GET.get('device'))
        context['color'] = get_object_or_404(Color, id=self.request.GET.get('color'))
        context['mem_size'] = get_object_or_404(MemSize, id=self.request.GET.get('mem_size'))
        if self.request.GET.get('email'):
            device_proxy = DeviceProxy()
            if self.request.GET.get('how_works') == 'fine':
                device_proxy.is_fully_exploitable = True
            elif self.request.GET.get('how_works') == 'bad':
                device_proxy.broken_screen = True
            device_proxy.damage = int(self.request.GET.get('damage'))
            device = get_object_or_404(Device, id=self.request.GET.get('device'))
            device = device.version_model.filter(
                color=self.request.GET.get('color'),
                mem_size=self.request.GET.get('mem_size'))
            context['recommended_price'] = WizardPrice(device=device.first(), device_proxy=device_proxy).recommended_price
        return context