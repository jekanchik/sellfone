from django.contrib import admin
from price_wizard.models import TablePrice
# Register your models here.

class TablePriceAdmin(admin.ModelAdmin):
    list_display = ('device', 'price', 'when_created')

admin.site.register(TablePrice, TablePriceAdmin)
