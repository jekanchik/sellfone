from django.conf.urls import url, include

from price_wizard import views

urlpatterns = [
    url(
        r'express_cost_estimation/$',
        views.ExpressCostView.as_view(),
        name='express_cost_estimation',
    ),
    url(
        r'express_cost_estimation/check-price$',
        views.ExpressCostView.as_view(),
        name='express_cost_estimation',
    )
    ]