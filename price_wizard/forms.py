# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from dal import autocomplete
from django import forms
from django.forms.widgets import Select
from django.utils.translation import ugettext as _

from devices.models import Device, Brand, DeviceType, Color, MemSize
from products.forms import Choices
from products.models import Product


class ExpressCostIndexForm(forms.Form):

    type = forms.ModelChoiceField(
        queryset=DeviceType.objects,
        label=_('Категория'),
        widget=forms.Select(
            attrs={
                'id': 'select-category',
                'class': 'phone-cheract model-ch',
            }
        ),
    )
    brand = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'id': 'select-brand',
                'class': 'phone-cheract model-ch'
            }
        ),
    )
    device = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': 'phone-cheract model-ch',
            }
        ),
    )
    color = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'id': 'id_color',
                'class': 'phone-cheract model-ch'
            }
        ),
    )
    mem_size = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'id': 'id_mem_size',
                'class': 'phone-cheract model-ch'
            }
        ),
    )

class ExpressCostForm(forms.ModelForm):
    type = forms.ModelChoiceField(
        queryset=DeviceType.objects,
        widget=forms.HiddenInput(
            attrs={
                'id': 'select-category',
                'class': 'phone-cheract model-ch',
            }
        ),
    )
    how_works = forms.ChoiceField(
        choices=Choices.WORK,
        widget=forms.RadioSelect(
            attrs={
                'class': 'how_works'
            }
        )
    )
    brand = forms.ModelChoiceField(
        queryset=Brand.objects,
        widget=forms.HiddenInput(
            attrs={
                'id': 'select-brand',
                'class': 'phone-cheract model-ch'
            }
        ),
    )
    device = forms.ModelChoiceField(
        queryset=Device.objects,
        widget=forms.HiddenInput(
            attrs={
                'class': 'phone-cheract model-ch',
            }
        ),
    )
    color = forms.ModelChoiceField(
        queryset=Color.objects,
        widget=forms.HiddenInput(
            attrs={
                'id': 'id_color',
                'class': 'phone-cheract model-ch'
            }
        ),
    )
    mem_size = forms.ModelChoiceField(
        queryset=MemSize.objects,
        widget=forms.HiddenInput(
            attrs={
                'id': 'id_mem_size',
                'class': 'phone-cheract model-ch'
            }
        ),
    )
    class Meta:
        model = Product
        fields = 'damage',
        widgets = {
            'damage': forms.RadioSelect(),
        }

    def __init__(self, *args, **kwargs):
        super(ExpressCostForm, self).__init__(*args, **kwargs)
        self.fields['damage'].choices = self.fields['damage'].choices[::-1]
