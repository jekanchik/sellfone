# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Territory(MPTTModel):
    class Meta:
        verbose_name = 'Территория'
        verbose_name_plural = 'Территории'

    def __unicode__(self):
        return self.name

    parent = TreeForeignKey('self', verbose_name='Входит в',
                            related_name='subterritories',
                            blank=True, null=True)
    name = models.CharField('Название', max_length=255)

    def get_address(self, delimiter=', '):
        addr = list(self.get_ancestors().values_list('name', flat=True))
        addr.append(self.name)
        return delimiter.join(addr)
