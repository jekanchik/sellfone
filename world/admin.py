# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from world.models import Territory


admin.site.register(Territory, MPTTModelAdmin)


class RootFilter(admin.RelatedFieldListFilter):
    def __init__(self, *args, **kwargs):
        super(RootFilter, self).__init__(*args, **kwargs)
        if getattr(self.__class__, 'title', None):
            self.title = self.__class__.title
        else:
            self.title = self.field.rel.to._meta.verbose_name

    def field_choices(self, field, request, model_admin):
        return [(t.tree_id, t.name)
                for t in field.rel.to.objects.root_nodes()]

    def queryset(self, request, queryset):
        if self.lookup_val:
            return queryset.filter(
                **{self.field.name + '__tree_id': self.lookup_val}
            )
        if self.lookup_val_isnull:
            return queryset.filter(**{self.field.name: None})
        return queryset


class CountryFilter(RootFilter):
    title = 'Страна'
