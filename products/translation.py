from modeltranslation.translator import register, TranslationOptions
from products.models import Product

@register(Product)
class NewsTranslationOptions(TranslationOptions):
    fields = ('name', 'device',)