# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template

from products.models import Product

register = template.Library()

@register.simple_tag(takes_context=True)
def notice_progress(context, request, float=None):
    progress = 20
    product = Product.objects.get(id=request.session['create_product'])
    if product.accessories:
        progress += 10
    if product.IMEI:
        progress += 10
    if product.purchase_country:
        progress += 5
    if product.has_guarantee:
        progress += 5
    if product.is_repaired:
        progress += 5
    if product.refurbished:
        progress += 5
    if product.owner:
        progress += 5
    if product.owner:
        progress += 5
    if product.description:
        progress += 15
    if product.extra_images.all():
        progress += 10
    if product.image:
        progress += 10
    if float:
        progress /= 100.0
    return progress