# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext as _
from django.utils.html import format_html

from haystack.forms import SearchForm
from haystack.query import SearchQuerySet

from .models import Product, ExtraImage
from devices.models import DeviceType, Device, MemSize, Color
from devices.models import Brand
from devices.models import Accessory


def label_html(title, text=''):
    return format_html(
        '<span class="main-text">{}</span><br>{}'.format(title, text))


class Choices:
    WORK = (
        ('fine', label_html('Полностью рабочий', 'устройство включается, батарея держит заряд, все кнопки и разъёмы функционируют должным образом')),
        ('worse', label_html('Телефон работает, но есть дефекты', 'западaющая или не работающая кнопка, не работающий сканер отпечатка, битые пиксели, неработающий разъем')),
        ('bad', label_html('Телефон не работает', 'телефон не включается, разбитый экран, батарея не держит заряд, не работают кнопки')),
    )
    NULL_BOOLEAN = (
        (True, 'Да'),
        (False, 'Нет'),
        (None, 'Не знаю'),
    )
    BOOLEAN = (
        (True, 'Да'),
        (False, 'Нет'),
    )
    EMPTY = (
        ('', '---------'),
        )


class VariantsForm(forms.Form):
    category = forms.ModelChoiceField(
        queryset=DeviceType.objects,
        label=_('Категория'),
        widget=forms.Select(
            attrs={
                'id': 'select-category',
                'class': 'phone-cheract model-ch',
            }
        ),
    )
    brand = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'id': 'select-brand',
                'class': 'phone-cheract model-ch'
            }
        ),
        choices=Choices.EMPTY
    )
    device = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': 'phone-cheract model-ch'
            }
        ),
        choices=Choices.EMPTY
    )
    color = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': 'phone-cheract model-ch'
            }
        ),
        choices=Choices.EMPTY
    )
    mem_size = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'id': 'id_mem_size',
                'class': 'phone-cheract model-ch'
            }
        ),
        choices=Choices.EMPTY
    )


class AddProductFirstForm(forms.ModelForm):

    category = forms.ModelChoiceField(
        queryset=DeviceType.objects,
        label=_('Категория'),
        widget=forms.Select(
            attrs={
                'id': 'select-category',
                'class': 'phone-cheract model-ch',
            }
        ),
    )
    brand = forms.ModelChoiceField(
        queryset=Brand.objects,
        label=_('Производитель'),
        widget=forms.Select(
            attrs={
                'id': 'select-brand',
                'class': 'phone-cheract model-ch'
            }
        ),
    )
    device = forms.ModelChoiceField(
        queryset=Device.objects,
        widget=forms.Select(
            attrs={
                'class': 'phone-cheract model-ch'
            }
        ),
    )
    mem_size = forms.ModelChoiceField(
        queryset=MemSize.objects,
        widget=forms.Select(
            attrs={
                'id': 'id_mem_size',
                'class': 'phone-cheract model-ch'
            }
        ),
    )
    color = forms.ModelChoiceField(
        queryset=Color.objects,
        widget=forms.Select(
            attrs={
                'class': 'phone-cheract model-ch'
            }
        )
    )
    how_works = forms.ChoiceField(
        choices=Choices.WORK,
        widget=forms.RadioSelect(
            attrs={
                'class': 'how_works'
            }
        )
    )
    accessories = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(),
        required=True,
        queryset=Accessory.objects.all(),
    )

    class Meta:
        model = Product
        fields = (
            'damage', 'accessories', 'IMEI',
            'owner', 'refurbished', 'has_guarantee', 'purchase_country',
            'extra_accessories', 'is_repaired', 'locked', 'device', 'color'
        )
        widgets = {
            'damage': forms.RadioSelect(),
            'color': forms.Select(
                attrs={
                    'class': 'phone-cheract model-ch'
                }),
            'extra_accessories': forms.TextInput(
                attrs={
                    'placeholder': 'Другое: если у Вас есть чехлы или ' + \
                    'прочие аксессуары, укажите их наличие в этой графе.'
                }),
            'IMEI': forms.TextInput(
                attrs={
                    'placeholder': 'Наберите на телефоне *#06#',
                    'class': 'phone-cheract addtnl-ch col-xs-6',
                    'data-toggle': 'popover',
                }),
            'has_guarantee': forms.RadioSelect(
                choices=Choices.BOOLEAN,
            ),
            'refurbished': forms.RadioSelect(
                choices=Choices.NULL_BOOLEAN,
            ),
            'is_repaired': forms.RadioSelect(
                choices=Choices.NULL_BOOLEAN,
            ),
            'owner': forms.RadioSelect(
                choices=Choices.BOOLEAN,
            ),
            'locked': forms.RadioSelect(
                choices=Choices.NULL_BOOLEAN,
            ),
            # 'flash_version': forms.TextInput(
            #     attrs={
            #         'placeholder': _('Версия прошивки'),
            #         'class': 'phone-cheract addtnl-ch col-xs-6',
            #     }
            # ),
        }

    def __init__(self, *args, **kwargs):
        self.brands = kwargs.pop('brands', None)
        self.devices = kwargs.pop('devices', None)
        self.colors = kwargs.pop('colors', None)
        self.mem_size = kwargs.pop('mem_size', None)
        super(AddProductFirstForm, self).__init__(*args, **kwargs)
        self.fields['device'].empty_label = None
        self.fields['color'].empty_label = None
        self.fields['purchase_country'].empty_label = None
        self.fields['purchase_country'].widget.attrs['class'] \
            = 'nice-select phone-cheract addtnl-ch col-xs-6'
        self.fields['has_guarantee'].initial = True
        self.fields['locked'].initial = False
        if self.brands is not None:
            self.fields['brand'].queryset = self.brands
        if self.devices is not None:
            self.fields['device'].queryset = self.devices
        if self.colors is not None:
            self.fields['color'].queryset = self.colors
        if self.mem_size is not None:
            self.fields['mem_size'].queryset = self.mem_size
        self.fields['damage'].choices = self.fields['damage'].choices[::-1]

    def save(self, commit=True):
        instance = super(AddProductFirstForm, self).save(commit=False)
        data = self.cleaned_data
        if data['how_works'] == 'fine':
            instance.is_fully_exploitable = True
            instance.is_switched_on = True
            instance.is_battery_holding_charge = True
            instance.is_buttons_functioning_properly = True
        elif data['how_works'] == 'worse':
            instance.is_buttons_functioning_properly = False
            instance.fingerprint_scanner_works = False
            instance.is_on_screen_dead_pixels = False
        elif data['how_works'] == 'bad':
            instance.is_fully_exploitable = False
            instance.is_switched_on = False
            instance.broken_screen = True
            instance.is_battery_holding_charge = False
            instance.is_buttons_functioning_properly = False
        if commit:
            instance.save()
        return instance


class AddProductSecondForm(forms.ModelForm):
    damaged_photos = forms.FileField(
        label=None,
        widget=forms.FileInput(
            attrs={
                'max_files': '6',
                'multiple': 'multiple',
            }),
        required=False
    )
    accessory_photos = forms.FileField(
        label=None,
        widget=forms.FileInput(
            attrs={
                'max_files': '3',
                'multiple': 'multiple',
            }),
        required=False
    )

    class Meta:
        model = Product
        fields = ('description', )
        widgets = {
            'description': forms.Textarea(
                attrs={
                    'max_length': 2000,
                    'cols': 15,
                    'class': "create-prod-descrp"
                }),
        }

    def __init__(self, *args, **kwargs):
        super(AddProductSecondForm, self).__init__(*args, **kwargs)


class AddProductThirdForm(forms.ModelForm):
    bargain = forms.TypedChoiceField(
        choices=((True, 'Да'), (False, 'Нет')),
        widget=forms.RadioSelect,
    )

    class Meta:
        model = Product
        fields = ('price', 'bargain')
        widgets = {
            'price': forms.TextInput(
                attrs={
                    'class': "myprice phone-cheract addtnl-ch",
                    'style': 'width: 90px',
                }),
        }


class AddProductImageForm(forms.ModelForm):
    image = forms.ImageField(
        widget=forms.FileInput(
            attrs={
                'class': 'required spec-file-input'
            }),
        required=False,
    )

    class Meta:
        model = ExtraImage
        fields = ('image',)
