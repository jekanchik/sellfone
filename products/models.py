# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.conf import settings
from django.db import models
from django_countries.fields import CountryField

from devices.models import Device, Accessory
from devices.models.specifications import Color
from users.models import User


class ProductManager(models.Manager):
    def published(self, *args, **kwargs):
        kwargs['status'] = self.model.STATUS.PUBLISHED
        return self.filter(*args, **kwargs)


class Product(models.Model):
    class STATUS:
        MODERATE = 0
        PUBLISHED = 1
        WAITING_FOR_PAYMENT = 2
        WAITING_FOR_SHIPMENT = 3
        WAITING_FOR_RECEIPT = 4
        RECEIVED = 5
        _CHOICES = (
            (MODERATE, _('На модерации')),
            (PUBLISHED, _('Опубликовано')),
            (WAITING_FOR_PAYMENT, _('Ожидается оплата')),
            (WAITING_FOR_SHIPMENT, _('Ожидается отправка')),
            (WAITING_FOR_RECEIPT, _('Ожидается подтверждение получения')),
            (RECEIVED, _('Получен'))
        )

    # name = models.CharField(_('Заголовок'), max_length=80)
    device = models.ForeignKey(Device, verbose_name=_('Устройство'),
                               related_name='products')
    user = models.ForeignKey(User, verbose_name=_('Пользователь'))
    price = models.DecimalField(_('Цена'), max_digits=9, decimal_places=2)
    color = models.ForeignKey(Color, verbose_name=_('Цвет'),
                              related_name='products', blank=True, null=True)
    refurbished = models.NullBooleanField(_('Восстановлено'), default=False)
    is_original = models.BooleanField(_('Оригинал'), default=False)
    is_reused = models.BooleanField(_('Б/У'), default=False)
    is_fully_exploitable = models.BooleanField(_('Полностью исправен'),
                                               default=False
                                               )
    is_repaired = models.NullBooleanField(_('Ремонтировался'), default=False)
    is_switched_on = models.BooleanField(_('устройство включается'),
                                         default=False
                                         )
    is_battery_holding_charge = models.BooleanField(_('Батарея держит заряд'),
                                                    default=False
                                                    )
    is_buttons_functioning_properly = models.BooleanField(
        _('Все кнопки функционируют должным образом'),
        default=False
    )
    is_on_screen_dead_pixels = models.BooleanField(
        _('На экране нет битых пикселей'),
        default=False
    )
    fingerprint_scanner_works = models.BooleanField(
        _('Cканер отпечатка работает'),
        default=False,
    )
    broken_screen = models.BooleanField(
        _('Разбитый экран'),
        default=False
    )
    has_scratches = models.BooleanField(_('Имеет царапины'), default=False)
    DAMAGE_TYPES = (
        (4,
         'Значительные следы износа, включая глубокие/большие царапины и трещины на экране, \n глубокие/большие вмятины, следы выгорания экрана (битых пикселей) и другие внешние дефекты'),
        (3, 'Много повреждений и заметных царапин на экране и корпусе, \n присутствуют вмятины'),
        (2,
         'Заметны следы использования. Есть незначительные повреждения и заметные царапины (без вмятин, царапины чётко видны)'),
        (1, 'Устройство в отличном состоянии, без явных следов использования, практически незаметные царапины'),
        (0, 'Телефон не использовался, в заводской упаковке'),
    )
    damage = models.IntegerField(
        _('Внешние повреждения устройства'),
        choices=DAMAGE_TYPES,
        default=0
    )
    accessories = models.ManyToManyField(
        Accessory,
        verbose_name=_('Аксессуары'),
        related_name='products',
        blank=True
    )
    extra_accessories = models.CharField(
        verbose_name=_('Дополнительные аксессуары'),
        max_length=100,
        null=True,
        blank=True,
    )
    owner = models.BooleanField(
        _('Вы первый владелец?'),
        default=True
    )
    IMEI = models.IntegerField(
        _('IMEI (*#06# кнопка вызова)'),
        null=True,
        blank=True)
    locked = models.NullBooleanField(
        _('Заблокировано под иностранного оператора?')
    )
    rooted = models.NullBooleanField(
        _('Root/jailbreak'),
    )
    flash_version = models.CharField(
        _('Версия прошивки'),
        max_length=255,
        null=True,
        blank=True
    )
    has_guarantee = models.NullBooleanField(
        _('Есть действующая гарантия?')
    )
    purchase_country = CountryField(
        verbose_name=_('Страна покупки'),
    )
    description = models.TextField(_('Описание'),
                                   blank=True, null=True)
    image = models.ImageField(_('Главное изображение'), upload_to='products')
    status = models.IntegerField(_('Статус'),
                                 choices=STATUS._CHOICES,
                                 default=STATUS.MODERATE)
    bargain = models.BooleanField(
        _('Торг уместен'),
        default=False,
    )
    when_created = models.DateTimeField(
        verbose_name=_('Дата создания'),
        auto_now_add=True
    )

    OPERABILITY_TYPES = (
        (1, 'Полностью рабочий'),
        (2, 'Рабочий, но есть дефекты'),
        (3, 'Не работает'),
    )

    how_works = models.IntegerField(
        _('Роботоспособность телефона'),
        choices=OPERABILITY_TYPES,
        blank=True,
        null=True
    )
    stolen = models.BooleanField(
        _('Украден?'),
        default=False
    )
    STATUS_CHECK = (
        (1, 'проверен'),
        (0, 'не проверен'),
    )
    sellfone_check = models.IntegerField(
        verbose_name='Статус технической проверки sellfone',
        choices=STATUS_CHECK,
        default=0
    )
    objects = ProductManager()

    def __unicode__(self):
        return '%s' % self.id

    def save(self, **kwargs):
        if not self.how_works:
            if self.is_fully_exploitable:
                self.how_works = 1
            elif self.broken_screen:
                self.how_works = 3
            else:
                self.how_works = 2
        else:
            if self.how_works == 1:
                self.is_fully_exploitable = True
                self.is_switched_on = True
                self.is_battery_holding_charge = True
                self.is_buttons_functioning_properly = True
            elif self.how_works == 2:
                self.is_buttons_functioning_properly = False
                self.fingerprint_scanner_works = False
                self.is_on_screen_dead_pixels = False
            elif self.how_works == 3:
                self.is_fully_exploitable = False
                self.is_switched_on = False
                self.broken_screen = True
                self.is_battery_holding_charge = False
                self.is_buttons_functioning_properly = False
        return super(Product, self).save( **kwargs)

    class Meta:
        verbose_name = _('Товар')
        verbose_name_plural = _('Товары')

    def title(self):
        return '%s, %s GB%s %s' % (self.device, self.device.mem_size.first(), ',' if self.color else '', self.color or '')

    def operability(self):
        if self.is_fully_exploitable:
            return _('Полностью рабочий')
        elif self.broken_screen:
            return _('Не работает')
        else:
            return _('Рабочий, но есть дефекты')

    def operability_detail(self):
        if self.is_fully_exploitable:
            return _('Полностью рабочий, устройство включается, батарея держит заряд, все кнопки и разъёмы функционируют должным образом')
        elif self.broken_screen:
            return _('Телефон не работает, телефон не включается, разбитый экран, батарея не держит заряд, не работают кнопки')
        else:
            return _('Телефон работает, но есть дефекты, западaющая или не работающая кнопка, не работающий сканер отпечатка, битые пиксели, неработающий разъем')

    def appearance(self):
        if self.damage == 0:
            return _('Новое')
        elif self.damage == 1:
            return _('Отличное')
        elif self.damage == 2:
            return _('Хорошее')
        elif self.damage == 3:
            return _('Удовлетворительное')
        elif self.damage == 4:
            return _('Дефектное')
        else:
            return ''

class ExtraImage(models.Model):
    image = models.ImageField(_('Изображение'))
    product = models.ForeignKey(Product,
                                verbose_name=_('Товар'),
                                related_name='extra_images')

    def __unicode__(self):
        return '%s' % self.image

    class Meta:
        verbose_name = _('Дополнительное изображение')
        verbose_name_plural = _('Дополнительные изображения')


class Purchase(models.Model):
    class PAYMENT:
        PAY_ON_DELIVERY = 'POD'
        CARD = 'card'
        _CHOICES = ((PAY_ON_DELIVERY, _('Наложенный платёж')),
                    (CARD, _('Банковская карта')))

    when_created = models.DateTimeField(_('Дата создания'), auto_now_add=True)
    purchaser = models.ForeignKey(settings.AUTH_USER_MODEL,
                                  verbose_name=_('Пользователь'),
                                  related_name='purchases')
    product = models.ForeignKey(Product, verbose_name=_('Товар'),
                                related_name='purchases')
    in_exchange_for = models.ForeignKey(Product, verbose_name=_('В обмен'),
                                        related_name='exchanges',
                                        blank=True, null=True)
    price = models.DecimalField(_('Цена'), max_digits=9, decimal_places=2,
                                blank=True)
    payment_method = models.CharField(_('Способ оплаты'), max_length=10,
                                      choices=PAYMENT._CHOICES)
    comment = models.TextField(_('Комментарий'), blank=True, null=True)

    class Meta:
        verbose_name = _('Покупка')
        verbose_name_plural = _('Покупки')

    def __unicode__(self):
        return '%s %s' % (_('Покупка'), self.product.name)

    def clean(self):
        if self.price is None:
            self.price = self.product.price
            if self.in_exchange_for:
                self.price -= self.in_exchange_for.price


class PurchaseReview(models.Model):
    scale = [(i, str(i)) for i in xrange(1, 6)]

    when_created = models.DateTimeField(_('Дата создания'), auto_now_add=True)
    purchase = models.OneToOneField(Purchase, verbose_name=_('Покупка'))
    overall = models.IntegerField(_('Оценка сделки'),
                                  choices=scale, default=3)
    compliance = models.IntegerField(_('Соответствие описанию'),
                                     choices=scale, default=3)
    fairness = models.IntegerField(_('Добросовестность продавца'),
                                   choices=scale, default=3)
    efficiency = models.IntegerField(_('Оперативность'),
                                     choices=scale, default=3)
    comment_compliance = models.TextField(
        verbose_name=_('Комментарий о соответствии описанию'),
        blank=True
    )
    comment_fairness = models.TextField(
        verbose_name=_('Комментарий о добросовестности'),
        blank=True
    )
    comment_efficiency = models.TextField(
        verbose_name=_('Комментарий об оперативности'),
        blank=True
    )

    class Meta:
        verbose_name = _('Отзыв о сделке')
        verbose_name_plural = _('Отзывы о сделках')

    def __unicode__(self):
        return '{}: {}'.format(self.purchase, self.get_total())

    def get_total(self):
        return 0.7 * self.overall + sum((self.compliance,
                                         self.fairness,
                                         self.efficiency)) / 10.0
