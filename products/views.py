# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import OrderedDict
from datetime import datetime, timedelta
from django.http import HttpResponse
from django.shortcuts import render, render_to_response
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.views.generic import DetailView, ListView
from django.http import JsonResponse
from django.forms import formset_factory
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from haystack.generic_views import SearchView

from price_wizard.forms import ExpressCostIndexForm
from products.models import Product
from products.models import ExtraImage
from products.forms import AddProductFirstForm
from products.forms import AddProductSecondForm
from products.forms import AddProductThirdForm
from products.forms import AddProductImageForm
from products.forms import VariantsForm
from products.decorators import check_step
from devices.models import DeviceType
from devices.models import Device
from devices.models import Brand

from price_wizard.wizard import WizardPrice


def index(request):
    form = ExpressCostIndexForm()
    return render(request, 'index.html', {'form': form})


class ProductListView(ListView):
    template_name = 'products/catalog.html'
    context_object_name = 'products_list'
    model = Product
    paginate_by = 20
    queryset = Product.objects.published()


class ProductDetailView(DetailView):
    model = Product
    context_object_name = 'product'
    template_name = 'products/product_detail.html'

    def get_object_or_404(self):
        obj = super(ProductDetailView, self).get_object()
        return obj

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        version_model = self.object.device.version_model.filter(
            mem_size=self.object.device.mem_size.first(),
            color=self.object.color,
        )
        last_month = datetime.today() - timedelta(days=30)
        try:
            context['chart'] = version_model.first().tab_price.filter(when_created__gte=last_month)
        except AttributeError:
            pass
        return context


def chart_period(request, period, pk):
    product = Product.objects.get(id=pk)
    try:
        version_model = product.device.version_model.filter(
            mem_size=product.device.mem_size.first(),
            color=product.color,
        )
        last_period = datetime.today() - timedelta(days=int(period))
        price = version_model.first().tab_price.filter(when_created__gte=last_period).order_by('when_created')
    except AttributeError:
        return HttpResponse(status=404)
    data = {}
    for i in price:
        when_created = i.when_created.strftime('%d.%m.%y')
        data[str(when_created)] = int(i.price)
    data = OrderedDict(sorted(data.items(), key=lambda t: t[0]))
    data = JsonResponse(data)
    return HttpResponse(data)


class SearchProductView(SearchView):
    template_name = 'products/catalog.html'
    context_object_name = 'products_list'

    def get_queryset(self):
        queryset = super(SearchProductView, self).get_queryset()
        return queryset.filter(status=Product.STATUS.PUBLISHED)


@login_required
@check_step
def add_product(request, step):
    context = {}
    recommended_price = 1
    variants_form = None
    if step == 1:
        variants_form = VariantsForm()
        product_form = AddProductFirstForm(request.POST or None)
        if product_form.is_valid():
            product = product_form.save(commit=False)
            product.price = 0
            product.user = request.user
            product.save()
            request.session['create_product'] = product.id
            request.session['last_step'] = 1
            if request.POST.get('save_exit'):
                return redirect('/')
            elif request.POST.get('delete'):
                product.delete()
                del request.session['create_product']
                del request.session['last_step']
                return redirect('/')
            else:
                return redirect_get(
                    'products:add', '?step={}'.format(step + 1))

    elif step == 2:
        product = get_object_or_404(
            Product,
            id=request.session.get('create_product', None))
        ImageFormSet = formset_factory(
            form=AddProductImageForm,
            extra=6)
        product_form = AddProductSecondForm(
            request.POST or None,
            request.FILES or None,
            instance=product)
        if request.method == 'POST':
            image_formset = ImageFormSet(request.POST, request.FILES)
            if product_form.is_valid() and image_formset.is_valid():

                product_form.save()
                request.session['last_step'] = 2
                for form in image_formset.cleaned_data:
                    if 'image' in form:
                        image = form['image']
                        photo = ExtraImage.objects.create(
                            product=product, image=image)
                        photo.save()
                damaged_photos = request.FILES.getlist('damaged_photos')
                accessory_photos = request.FILES.getlist('accessory_photos')
                for image in damaged_photos and accessory_photos:
                    ExtraImage.objects.create(
                        image=image,
                        product=product
                    )
                for image in accessory_photos:
                    ExtraImage.objects.create(
                        image=image,
                        product=product
                    )
                if request.POST.get('save_exit'):
                    return redirect('/')
                elif request.POST.get('delete'):
                    product.delete()
                    del request.session['create_product']
                    del request.session['last_step']
                    return redirect('/')
                else:
                    return redirect_get(
                        'products:add', '?step={}'.format(step + 1))
        context.update({
            'image_formset': ImageFormSet
        })
    elif step == 3:
        product = get_object_or_404(
            Product,
            id=request.session.get('create_product', None))
        recommended_price = WizardPrice(product=product).recommended_price
        product_form = AddProductThirdForm(
            request.POST or None, instance=product)
        if product_form.is_valid():
            product_form.save()
            del request.session['create_product']
            del request.session['last_step']
            if request.POST.get('save_exit'):
                return redirect('/')
            elif request.POST.get('save_preview'):
                return redirect('products:product_detail', pk=product.pk)
            elif request.POST.get('delete'):
                product.delete()
                return redirect('/')
            else:
                return redirect('/')  # link to profile with products in future
    context.update({
        'variants_form': variants_form,
        'product_form': product_form,
        'template': 'products/include/add_step{}.html'.format(step),
        'step': step,
        'recommended_price': int(round(recommended_price, -1)),
        'min_price': int(round(recommended_price / 2, -1)),
        'max_price': int(round(recommended_price * 1.5, -1)),
    })
    return render(request, 'products/add_product.html', context)


def get_parameters(request):
    if request.is_ajax():
        response = {}
        type = request.GET.get('type')
        value = request.GET.get('value')
        if type == 'category':
            category = get_object_or_404(DeviceType, id=value)
            brands = Brand.objects.filter(devices__type=category).distinct()
            devices = Device.objects.filter(type=category)
            product_form = AddProductFirstForm(brands=brands, devices=devices)
            brands_markup = product_form.fields['brand'] \
                .widget.render_options(selected_choices=[])
            devices_markup = product_form.fields['device'] \
                .widget.render_options(selected_choices=[])
            response.update(
                {'brands': brands_markup}
            )
        elif type == 'brand':
            brand = get_object_or_404(Brand, id=value)
            devices = Device.objects.filter(brand=brand)
            product_form = AddProductFirstForm(devices=devices)
            devices_markup = product_form.fields['device'] \
                .widget.render_options(selected_choices=[])
            response.update(
                {'devices': devices_markup}
            )
        elif type == 'device':
            device = get_object_or_404(Device, id=value)
            product_form = AddProductFirstForm(
                colors=device.colors.all(),
                mem_size=device.mem_size.all())
            colors_markup = product_form.fields['color'] \
                .widget.render_options(selected_choices=[])
            mem_size_markup = product_form.fields['mem_size'] \
                .widget.render_options(selected_choices=[])
            device_html = render_to_string(
                'products/include/device_short_info.html', {'device': device})
            response.update({
                'device_card': device_html,
                'colors': colors_markup,
                'mem_size': mem_size_markup
            })
    return JsonResponse(response)


def redirect_get(url_name, get_parameters):
    response = redirect('products:add')
    response['Location'] += get_parameters
    return response


