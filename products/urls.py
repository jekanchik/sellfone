
from django.conf.urls import url, include
from products import views
# from .views import AddProductView
from products.views import SearchProductView, chart_period

urlpatterns = [
    url(r'^search/$', SearchProductView.as_view(), name='product_search'),
    url(
        r'^$',
        views.ProductListView.as_view(),
        name='products'
    ),
    url(
        r'^product_detail/(?P<pk>\d+)/$',
        views.ProductDetailView.as_view(),
        name='product_detail'
    ),
    url(
        r'add/$',
        views.add_product,
        name='add',
    ),
    url(
        r'get_parameters/$',
        views.get_parameters,
        name='get_parameters',
    ),
    url(r'period-chart/(?P<period>\d+)/(?P<pk>\d+)/$', chart_period, name='chart_period')
]
