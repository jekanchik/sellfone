import datetime
from haystack import indexes
from products.models import Product


class ProductIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    when_created = indexes.DateTimeField(model_attr='when_created')
    price = indexes.DecimalField(model_attr='price')
    title = indexes.CharField(model_attr='title')
    image = indexes.CharField(model_attr='image')
    appearance = indexes.CharField(model_attr='appearance')
    operability = indexes.CharField(model_attr='operability')
    status = indexes.IntegerField(model_attr='status')

    def get_model(self):
        return Product

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(when_created__lte=datetime.datetime.now())