# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import redirect


def check_step(function):
    def wrap(request, *args, **kwargs):
        last_step = request.session.get('last_step', None)
        response = redirect('products:add')
        current_step = int(request.GET.get('step', 1))
        if last_step:
            if current_step > int(last_step) + 1:
                response['Location'] += \
                    '?step={}'.format(str(int(last_step) + 1))
                return response
        elif not last_step and current_step > 1:
            response['Location'] += \
                '?step=1'
            return response
        return function(request, step=current_step, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
