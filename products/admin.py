# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.shortcuts import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext as _
from modeltranslation.admin import TranslationAdmin

from devices.models import Device
from .models import Product, ExtraImage, Purchase, PurchaseReview


class ChangeUrl(object):
    def __init__(self, field, descr=None):
        self.field = field
        self.short_description = descr

    def __call__(self, obj):
        obj = getattr(obj, self.field)
        if obj is None:
            return '-'
        url = reverse('admin:%s_%s_change' % (obj._meta.app_label,
                                              obj._meta.model_name),
                      args=(obj.pk,))
        return format_html('<a href="{}">{}</a>', url, unicode(obj))


class ExtraImageInline(admin.TabularInline):
    model = ExtraImage
    extra = 0


# class DeviceInlineAdmin(admin.TabularInline):
#     model = Device




class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'when_created', 'device', 'price', 'status')
    list_select_related = True
    readonly_fields = 'when_created',
    list_filter = ('status', 'refurbished', 'owner', 'is_original',
                   'is_reused', 'is_fully_exploitable', 'is_repaired',
                   'is_switched_on', 'is_battery_holding_charge',
                   'is_buttons_functioning_properly',
                   'is_on_screen_dead_pixels',
                   'has_scratches', 'damage', 'locked', 'rooted')
    filter_horizontal = ('accessories',)

    inlines = [ExtraImageInline]
    fieldsets = (
        (None, {'fields': ('device', 'color', 'price', 'status', 'image')}),
        (_('Информация'), {'fields': (
            'damage', 'how_works', 'IMEI','has_guarantee', 'is_repaired', 'refurbished', 'owner',
            'purchase_country', 'locked', 'stolen',
            'sellfone_check', 'extra_accessories', 'accessories', 'description', 'user',
        )}),
        # (_('Тех.Информация'), {'fields': ('owner', 'IMEI', 'locked', 'rooted',
        #                               'flash_version', 'accessories',
        #                               'description', 'image', 'user')})
    )

admin.site.register(Product, ProductAdmin)


class PurchaseAdmin(admin.ModelAdmin):
    list_display = ('when_created', 'purchaser', 'product', 'in_exchange_for',
                    'price', 'payment_method')
    list_filter = ('payment_method',)
    fields = ('when_created', 'purchaser', ('product', 'in_exchange_for'),
              ('price', 'payment_method'), 'comment')
    readonly_fields = ('when_created',)
admin.site.register(Purchase, PurchaseAdmin)


class PurchaseReviewAdmin(admin.ModelAdmin):
    list_display = ('when_created', 'purchase_url', 'overall', 'compliance',
                    'fairness', 'efficiency', 'total')
    fieldsets = (
        (None, {'fields': ('when_created', 'purchase',
                           'overall', 'compliance', 'fairness', 'efficiency',
                            'total')}),
        (_('Комментарии'), {'fields': ('comment_compliance',
                                       'comment_fairness',
                                       'comment_efficiency'),
                            'classes': ('collapse',)})
    )
    readonly_fields = ('when_created', 'total',)

    purchase_url = ChangeUrl('purchase', descr=_('Покупка'))

    def total(self, obj):
        return obj.get_total()
    total.short_description = _('Общая оценка')
admin.site.register(PurchaseReview, PurchaseReviewAdmin)
