from __future__ import division


def humanize(value, units='KMGTP', base=1000):
    if value < base:
        return value, ''
    next_ = base
    for unit in units:
        curr = next_
        next_ = curr * base
        if value < next_:
            break
    return value / curr, unit


def humanize_bytes(value):
    value, unit = humanize(value, base=1024)
    return value, unit + 'B'
