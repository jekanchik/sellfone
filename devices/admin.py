# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext as _
from mptt.admin import MPTTModelAdmin

from world.admin import RootFilter
from devices.models import (
    OS, Brand, DeviceType, Device, ExtraFeatureIntValue, ExtraFeatureTextValue,
    ExtraFeatureCharValue, ExtraFeatureBoolValue, ExtraFeatureFloatValue,
    Accessory,
    VersionModel)
from devices.models.specifications import (Display, DisplayTech, Color,
                                           Processor, SimCardType, NetGen,
                                           Material, Protection, Sensor,
                                           GraphicAccelerator, Camera,
                                           FlashType, Interface, ExtraFeature,
                                           MemSize)


@admin.register(ExtraFeature)
class ExtraFeatureAdmin(admin.ModelAdmin):
    list_display = ('name', 'group')
    list_filter = ('group',)


class ExtraFeatureValueInline(admin.TabularInline):
    extra = 0


extra_feature_inlines = [
    type(model.__name__ + str('Inine'),
         (ExtraFeatureValueInline,),
         {'model': model}) for model in (ExtraFeatureIntValue,
                                         ExtraFeatureFloatValue,
                                         ExtraFeatureBoolValue,
                                         ExtraFeatureCharValue,
                                         ExtraFeatureTextValue)]


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = ('name', 'brand', 'type', 'os', 'price')
    list_filter = ('type', 'os', ('brand', RootFilter))
    filter_horizontal = (
        'sim_card_types', 'colors', 'materials', 'interfaces', 'sensors',
        'net_generations', 'mem_card_types',
    )
    fieldsets = (
        (None, {'fields': ('type', 'brand', 'name', 'os', 'image',
                           'release_year', 'price'),}),
        (_('Технические характеристики'), {'fields': ('cpu', 'gpu', 'ram_size',
                                                      'display', 'sensors'),
                                           'classes': ('collapse',)}),
        (_('Камера'), {'fields': ('cam_main', 'cam_front')}),
        (_('Память'), {'fields': ('mem_size', 'has_mem_card_slot',
                                  'mem_card_types', 'mem_card_max'),
                       'classes': ('collapse',)}),
        (_('Мобильная связь'), {'fields': ('sim_cards_count', 'sim_card_types',
                                           'net_generations'),
                                'classes': ('collapse',)}),
        (_('Интерфейсы'), {'fields': ('interfaces',),
                           'classes': ('collapse',)}),
        (_('Корпус'), {'fields': (('dimen_x', 'dimen_y', 'dimen_z'),
                                  'protection', 'colors', 'materials',
                                  'weight'),
                       'classes': ('collapse',)}),
        (_('Питание'), {'fields': ('acc_capacity',
                                   ('time_active', 'time_wait'),
                                   'wireless_charge')})
    )
    inlines = extra_feature_inlines


@admin.register(Processor)
class ProcessorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'frequency_hr', 'cores')

    def frequency_hr(self, obj):
        return obj.get_frequency_hr()

    frequency_hr.short_description = 'Частота'


@admin.register(Display)
class DisplayAdmin(admin.ModelAdmin):
    list_display = ('id', 'diag', 'res_w', 'res_h', 'tech')
    list_filter = ('tech',)


@admin.register(Camera)
class CameraAdmin(admin.ModelAdmin):
    list_display = ('id', 'sensor_res', 'max_photo_res_w', 'max_photo_res_h',
                    'max_video_res_w', 'max_video_res_h', 'video_frame_rate',
                    'autofocus', 'flash', 'flash_type')
    list_filter = ('autofocus', 'flash_type')
    fields = ('sensor_res', ('max_photo_res_w', 'max_photo_res_h'),
              ('max_video_res_w', 'max_video_res_h'), 'video_frame_rate',
              'autofocus', ('flash', 'flash_type'), 'additional_info')


@admin.register(Interface)
class InterfaceAdmin(MPTTModelAdmin):
    list_display = ('name', 'type')
    list_filter = ('type', ('parent', RootFilter))


admin.site.register(Brand, MPTTModelAdmin)
for model in (DeviceType, OS, DisplayTech, Color, SimCardType, NetGen, Sensor,
              Material, Protection, GraphicAccelerator, FlashType, Accessory, MemSize):
    admin.site.register(model)


@admin.register(VersionModel)
class VersionModelAdmin(admin.ModelAdmin):
    list_display = ('device', 'color', 'mem_size')
