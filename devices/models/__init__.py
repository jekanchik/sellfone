# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from os import path

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _
from mptt.models import MPTTModel, TreeForeignKey

from devices.models.specifications import (Processor, Color, Display, NetGen,
                                           SimCardType, MemCardType, Material,
                                           Protection, Sensor, Camera,
                                           GraphicAccelerator, Interface,
                                           ExtraFeature, MemoryIn, MemSize)


DEVICES_MEDIA_ROOT = getattr(settings, 'DEVICES_MEDIA_ROOT', 'catalog')


class OS(models.Model):
    class Meta:
        verbose_name = _('Операционная система')
        verbose_name_plural = _('Операционные системы')

    def __unicode__(self):
        return self.name

    name = models.CharField(_('Название'), max_length=70)
    image = models.ImageField(_('Изображение'),
                              upload_to=path.join(DEVICES_MEDIA_ROOT, 'os'))


class Brand(MPTTModel):
    class Meta:
        verbose_name = _('Бренд')
        verbose_name_plural = _('Бренды')

    def __unicode__(self):
        return self.name

    parent = TreeForeignKey('self', verbose_name=_('Родительский бренд'),
                            related_name='subbrands', blank=True, null=True)
    name = models.CharField(_('Название'), max_length=70)


class DeviceType(models.Model):
    class Meta:
        verbose_name = _('Тип устройства')
        verbose_name_plural = _('Типы устройств')

    def __unicode__(self):
        return self.name

    name = models.CharField(_('Название'), max_length=140)
    image = models.ImageField(
        verbose_name=_('Изображение'),
        upload_to=path.join(DEVICES_MEDIA_ROOT, 'types')
    )


class Device(models.Model):
    class Meta:
        verbose_name = _('Устройство')
        verbose_name_plural = _('Устройства')

    def __unicode__(self):
        return self.name

    os = models.ForeignKey(OS, verbose_name=_('Операционная система'),
                           related_name='devices', blank=True, null=True)
    brand = models.ForeignKey(Brand, verbose_name=_('Бренд'),
                              related_name='devices')
    type = models.ForeignKey(DeviceType, verbose_name=_('Тип устройства'),
                             related_name='devices')
    name = models.CharField(_('Название'), max_length=255)
    image = models.ImageField(
        verbose_name=_('Изображение'),
        upload_to=path.join(DEVICES_MEDIA_ROOT, 'devices')
    )
    release_year = models.PositiveIntegerField(_('Год выхода'),
                                               blank=True, null=True)
    price = models.DecimalField(_('Цена'), decimal_places=2, max_digits=9)

    display = models.ForeignKey(Display, verbose_name=_('Дисплей'),
                                related_name='devices', blank=True, null=True)
    cpu = models.ForeignKey(Processor, verbose_name=_('Центральный процессор'),
                            related_name='devices', blank=True, null=True)
    gpu = models.ForeignKey(GraphicAccelerator, blank=True, null=True,
                            verbose_name=_('Графический ускоритель'),
                            related_name='devices')
    ram_size = models.IntegerField(_('Объём оперативной памяти'),
                                   blank=True, null=True)
    sensors = models.ManyToManyField(Sensor, verbose_name=_('Датчики'),
                                     related_name='devices', blank=True)
    cam_main = models.ForeignKey(Camera, verbose_name=_('Основная камера'),
                                 related_name='devices_main',
                                 blank=True, null=True)
    cam_front = models.ForeignKey(Camera, verbose_name=_('Фронтальная камера'),
                                  related_name='devices_front',
                                  blank=True, null=True)

    # storage
    # mem_size = models.CharField(
    #     _('Объём встроенной памяти'),
    #     choices=MemoryIn.CHOICES,
    #     max_length=10,
    #     blank=True,
    #     null=True)
    mem_size = models.ManyToManyField(
        MemSize,
        verbose_name=_('Объём встроенной памяти'),
        related_name='devices',
        blank=True)
    has_mem_card_slot = models.NullBooleanField(_('Слот для карты памяти'))
    mem_card_types = models.ManyToManyField(MemCardType,
                                            verbose_name=_('Типы карт памяти'),
                                            related_name='devices', blank=True)
    mem_card_max = models.IntegerField(_('Максимальный объём карты памяти'),
                                       blank=True, null=True)

    # mobile connection
    sim_cards_count = models.PositiveIntegerField(_('Количество сим-карт'),
                                                  blank=True, null=True)
    sim_card_types = models.ManyToManyField(
        SimCardType,
        verbose_name=_('Поддерживаемые сим-карты'),
        related_name='devices',
        blank=True
    )
    net_generations = models.ManyToManyField(
        NetGen,
        verbose_name=_('Поколения мобильной связи'),
        related_name='devices',
        blank=True
    )

    interfaces = models.ManyToManyField(Interface, blank=True,
                                        verbose_name=_('Интерфейсы'),
                                        related_name='devices')

    # body
    colors = models.ManyToManyField(Color, verbose_name=_('Варианты цвета'),
                                    related_name='devices', blank=True)
    dimen_x = models.PositiveIntegerField(_('Ширина'), blank=True, null=True,
                                          help_text=_('В миллиметрах'))
    dimen_y = models.PositiveIntegerField(_('Высота'), blank=True, null=True,
                                          help_text=_('В миллиметрах'))
    dimen_z = models.PositiveIntegerField(_('Толщина'), blank=True, null=True,
                                          help_text=_('В миллиметрах'))
    weight = models.PositiveIntegerField(_('Вес'), blank=True, null=True,
                                         help_text=_('В граммах'))
    materials = models.ManyToManyField(Material, verbose_name=_('Материалы'),
                                       related_name='devices', blank=True)
    protection = models.ForeignKey(Protection, blank=True, null=True,
                                   verbose_name=_('Стандарт защищённости'),
                                   related_name='devices')

    # power
    acc_capacity = models.PositiveIntegerField(_('Ёмкость аккумулятора'),
                                               blank=True, null=True,
                                               help_text=_('В мАч'))
    time_active = models.PositiveIntegerField(
        verbose_name=_('Время активного использования'),
        help_text=_('В часах'), blank=True, null=True
    )
    time_wait = models.PositiveIntegerField(
        verbose_name=_('Время в режиме ожидания'),
        help_text=_('В часах'), blank=True, null=True
    )
    wireless_charge = models.NullBooleanField(_('Беспроводная зарядка'))

    def extra_values(self):
        for model in (ExtraFeatureIntValue,
                      ExtraFeatureFloatValue,
                      ExtraFeatureBoolValue,
                      ExtraFeatureCharValue,
                      ExtraFeatureTextValue):
            for item in model.objects.filter(device=self)\
                    .select_related('feature').iterator():
                yield item

    def get_dimen_all(self):
        return '%sx%sx%s' % (self.dimen_x, self.dimen_y, self.dimen_z)


class Accessory(models.Model):
    name = models.CharField(_('Название'), max_length=255)

    class Meta:
        verbose_name = _('Аксессуар')
        verbose_name_plural = _('Аксессуары')

    def __unicode__(self):
        return self.name


class ExtraFeatureValue(models.Model):
    feature = models.ForeignKey(ExtraFeature, verbose_name=_('Особенность'))
    device = models.ForeignKey(Device, verbose_name=_('Устройство'))

    class Meta:
        abstract = True

    def __unicode__(self):
        return '{}: {}'.format(self.feature.name, self.value)


class ExtraFeatureIntValue(ExtraFeatureValue):
    value = models.IntegerField(_('Значение'))

    class Meta:
        verbose_name = _('Целочисленное значение')
        verbose_name_plural = _('Целочисленные значения')


class ExtraFeatureFloatValue(ExtraFeatureValue):
    value = models.FloatField(_('Значение'))

    class Meta:
        verbose_name = _('Дробное значение')
        verbose_name_plural = _('Дробные значения')


class ExtraFeatureBoolValue(ExtraFeatureValue):
    value = models.BooleanField(_('Значение'))

    class Meta:
        verbose_name = _('Бинарное значение')
        verbose_name_plural = _('Бинарные значения')


class ExtraFeatureCharValue(ExtraFeatureValue):
    value = models.CharField(_('Значение'), max_length=255)

    class Meta:
        verbose_name = _('Строковое значение')
        verbose_name_plural = _('Строковые значения')


class ExtraFeatureTextValue(ExtraFeatureValue):
    value = models.TextField(_('Значение'))

    class Meta:
        verbose_name = _('Текстовое значение')
        verbose_name_plural = _('Текстовые значения')


class VersionModel(models.Model):

    device = models.ForeignKey(
        Device,
        verbose_name=_('Устройство'),
        related_name='version_model',
    )
    color = models.ForeignKey(
        Color,
        verbose_name=_('Цвет'),
        related_name='version_model',
        blank=True,
        null=True
    )
    mem_size = models.ForeignKey(
        MemSize,
        verbose_name=_('Объём встроенной памяти'),
        related_name='version_model',
        blank=True
    )
    image = models.ImageField(
        verbose_name=_('Изображение'),
        upload_to=path.join(DEVICES_MEDIA_ROOT, 'version_model')
    )

    class Meta:
        verbose_name = _('Вариант модели')
        verbose_name_plural = _('Варианты модели')

    def __unicode__(self):
        return '{0}, {1}, {2}'.format(self.device, self.color, self.mem_size)
