# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _
from mptt.models import MPTTModel, TreeForeignKey

from devices.utils import humanize


class NamedItem(models.Model):
    name_length = 70

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.name

    name = models.CharField(_('Название'), max_length=name_length)


class DisplayTech(NamedItem):
    name_length = 30

    class Meta:
        verbose_name = _('Технология изготовления экрана')
        verbose_name_plural = _('Технологии изготовления экрана')


class Display(models.Model):
    class Meta:
        verbose_name = _('Дисплей')
        verbose_name_plural = _('Дисплеи')

    def __unicode__(self):
        params = filter(None, [self.get_diag_hr(), self.get_resolution_hr()])
        if self.tech:
            params.append(self.tech.name)
        if params:
            return ', '.join(params)
        return 'Display #%i' % self.id

    diag = models.DecimalField(_('Диагональ'), decimal_places=1, max_digits=4,
                               blank=True, null=True, help_text=_('В дюймах'))
    res_w = models.PositiveIntegerField(_('Разрешение, ширина'),
                                        blank=True, null=True)
    res_h = models.PositiveIntegerField(_('Разрешение, высота'),
                                        blank=True, null=True)
    tech = models.ForeignKey(DisplayTech, verbose_name=_('Технология'),
                             related_name='displays', blank=True, null=True)
    is_color = models.NullBooleanField(_('Цветной экран'))
    is_touch = models.NullBooleanField(_('Сенсорный экран'))

    # "hr" is "human readable"
    def get_diag_hr(self):
        if self.diag:
            return '{}"'.format(self.diag)
        return ''

    def get_resolution_hr(self):
        if self.res_w and self.res_h:
            return '%i x %i' % (self.res_w, self.res_h)
        return ''


class Processor(models.Model):
    class Meta:
        verbose_name = _('Процессор')
        verbose_name_plural = _('Процессоры')

    def __unicode__(self):
        if self.name:
            return self.name
        if self.cores and self.frequency:
            return '%i x %s' % (self.cores, self.get_frequency_hr())
        if self.cores or self.frequency:
            return self.get_frequency_hr() or _('%i-ядерный') % self.cores
        return 'Proc #%i' % self.id

    name = models.CharField(_('Название'), max_length=255,
                            blank=True, null=True)
    frequency = models.PositiveIntegerField(_('Частота'),
                                            help_text=_('В герцах'),
                                            blank=True, null=True)
    cores = models.PositiveIntegerField(_('Количество ядер'), default=2,
                                        blank=True, null=True)

    def get_frequency_hr(self):
        if self.frequency:
            return '{} {}Hz'.format(*humanize(self.frequency))
        return ''


class Color(NamedItem):
    class Meta:
        verbose_name = _('Цвет')
        verbose_name_plural = _('Цвета')


class MemSize(NamedItem):
    class Meta:
        verbose_name = _('Объём встроенной памяти')
        verbose_name_plural = _('Объёмы встроенной памяти')


class SimCardType(NamedItem):
    name_length = 30

    class Meta:
        verbose_name = _('Тип сим-карт')
        verbose_name_plural = _('Типы сим-карт')


class MemCardType(NamedItem):
    name_len = 30

    class Meta:
        verbose_name = _('Тип карты памяти')
        verbose_name_plural = _('Типы карт памяти')


class NetGen(NamedItem):
    name_length = 20

    class Meta:
        verbose_name = _('Поколение мобильной связи')
        verbose_name_plural = _('Поколения мобильной связи')


class Material(NamedItem):
    name_length = 255

    class Meta:
        verbose_name = _('Материал')
        verbose_name_plural = _('Материалы')


class Protection(NamedItem):
    class Meta:
        verbose_name = _('Стандарт защищённости')
        verbose_name_plural = _('Стандарты защищённости')


class Sensor(NamedItem):
    class Meta:
        verbose_name = _('Датчик')
        verbose_name_plural = _('Датчики')


class GraphicAccelerator(NamedItem):
    name_length = 255

    class Meta:
        verbose_name = _('Графический ускоритель')
        verbose_name_plural = _('Графические ускорители')


class FlashType(NamedItem):
    class Meta:
        verbose_name = _('Тип вспышки')
        verbose_name_plural = _('Типы вспышек')


class Camera(models.Model):
    sensor_res = models.PositiveIntegerField(
        verbose_name=_('Разрешение сенсора'),
        help_text=_('В мегапикселях'),
        blank=True, null=True
    )
    max_photo_res_w = models.PositiveIntegerField(
        verbose_name=_('Фото, ширина'),
        help_text=_('Максимальное разрешение'),
        blank=True, null=True
    )
    max_photo_res_h = models.PositiveIntegerField(
        verbose_name=_('Фото, высота'),
        help_text=_('Максимальное разрешение'),
        blank=True, null=True
    )
    max_video_res_w = models.PositiveIntegerField(
        verbose_name=_('Видео, ширина'),
        help_text=_('Максимальное разрешение'),
        blank=True, null=True
    )
    max_video_res_h = models.PositiveIntegerField(
        verbose_name=_('Видео, высота'),
        help_text=_('Максимальное разрешение'),
        blank=True, null=True
    )
    video_frame_rate = models.PositiveIntegerField(
        verbose_name=_('Частота кадров'),
        help_text=_('Кадров в секунду в видео'),
        blank=True, null=True
    )
    autofocus = models.NullBooleanField(_('Автофокусировка'))
    flash = models.NullBooleanField(_('Есть вспышка'))
    flash_type = models.ForeignKey(FlashType, verbose_name=_('Тип вспышки'),
                                   blank=True, null=True)
    additional_info = models.TextField(_('Дополнительная информация'),
                                       blank=True, null=True)

    class Meta:
        verbose_name = _('Камера')
        verbose_name_plural = _('Камеры')

    def __unicode__(self):
        return ', '.join(self)

    def __iter__(self):
        if self.sensor_res:
            yield '%iM' % self.sensor_res
        if self.max_photo_res_w and self.max_photo_res_h:
            yield '%ix%i' % (self.max_photo_res_w, self.max_photo_res_h)
        if self.max_video_res_w and self.max_video_res_h:
            yield '%ix%i' % (self.max_video_res_w, self.max_video_res_h)
        if self.video_frame_rate:
            yield '%i %s' % (self.video_frame_rate, _('кадров в секунду'))
        if self.autofocus:
            yield _('автофокусировка')
        if self.flash_type:
            yield '%s вспышка' % self.flash_type.name.lower()
        elif self.flash:
            yield _('вспышка')

    def has_flash(self):
        return self.flash or self.flash_type is not None


class Interface(MPTTModel):
    class TYPE:
        WIRED = 1
        WIRELESS = 2
        _CHOICES = ((WIRED, _('Проводной')),
                    (WIRELESS, _('Беспроводной')))

    name = models.CharField(_('Название'), max_length=70)
    type = models.IntegerField(_('Тип'), choices=TYPE._CHOICES,
                               default=TYPE.WIRELESS)
    parent = TreeForeignKey('self', verbose_name=_('Родитель'),
                            related_name='versions', blank=True, null=True)

    class Meta:
        verbose_name = _('Интерфейс')
        verbose_name_plural = _('Интерфейсы')

    def __unicode__(self):
        return self.name


class ExtraFeature(models.Model):
    class GROUP:
        NULL = ''
        STORAGE = 'storage'
        MOBILE_CONN = 'mobileconn'
        BODY = 'body'
        POWER = 'power'
        _CHOICES = ((NULL, _('Нет')),
                    (STORAGE, _('Память')),
                    (MOBILE_CONN, _('Мобильная связь')),
                    (BODY, _('Корпус')),
                    (POWER, _('Питание')))

    group = models.CharField(_('Группа'), max_length=20, default='',
                             choices=GROUP._CHOICES, blank=True)
    name = models.CharField(_('Название'), max_length=100)

    class Meta:
        verbose_name = _('Дополнительная особенность')
        verbose_name_plural = _('Дополнительные особенности')

    def __unicode__(self):
        return self.name


class MemoryIn(object):
    GB1 = '1'
    GB2 = '2'
    GB4 = '4'
    GB8 = '8'
    GB16 = '16'
    GB32 = '32'
    GB64 = '64'
    GB128 = '128'
    CHOICES = (
        (GB1, '1'),
        (GB2, '2'),
        (GB4, '4'),
        (GB8, '8'),
        (GB16, '16'),
        (GB32, '32'),
        (GB64, '64'),
        (GB128, '128'),
    )
