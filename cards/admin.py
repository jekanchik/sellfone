# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import CreditCard


class CreditCardAdmin(admin.ModelAdmin):
    list_display = (u'id', u'card_type', u'number')
admin.site.register(CreditCard, CreditCardAdmin)
