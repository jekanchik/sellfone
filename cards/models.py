# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models


class CreditCard(models.Model):
    VISA = 0
    MASTERCARD = 1
    CARD_TYPES = (
        (VISA, 'Visa'), 
        (MASTERCARD, 'Mastercard'),
    )
    card_type = models.IntegerField(verbose_name='Тип карты',
                                    choices=CARD_TYPES,
                                    default=VISA,
                                    null=True,
                                    blank=True)
    number = models.BigIntegerField(
        verbose_name='Номер карты'
    )
